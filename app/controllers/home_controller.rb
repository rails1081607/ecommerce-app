
# inherit from ApplicationController, index view route (/views/home/index)
class HomeController < ApplicationController
  def index
    # fetch first four records from the Category model and assign to main_categories var
    @main_categories = Category.take(4)
  end
end