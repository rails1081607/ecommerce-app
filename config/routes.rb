Rails.application.routes.draw do
  namespace :admin do
    resources :categories
  end
  devise_for :admins # adds a bunch of routes /admins/sign_in etc

  get "up" => "rails/health#show", as: :rails_health_check

  # Defines the root path route ("/")
  root "home#index"

  # authenticated users re-direct to /admin
  authenticated :admin_user do
    root to: "admin#index", as: :admin_root
  end

  get "admin" => "admin#index" # admin controller & admin index route
end
